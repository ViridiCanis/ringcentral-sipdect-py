# Ringcentral - SIPDECT Integration

## Files

index.py: main program, server
xml\_interface.py: helper functions for generation xml for the terminal interface
axi\_client.py: class for managing the AXI connection
.env: configuration

## Server

using FastAPI and RingCentral's SDK

### Endpoints

`/user/user_common.cfg`
- responds with `user_command.cfg` file in response to the OMM's request

`/user/user.cfg`
- when a new user logs in, this will collect the neccesary data from RC to create a new user on the OMM
- calls RC: GET "/rest../~/device", GET "/rest../extension/~", GET "/rest../device/{id}/sip-info"

`/messages`
- returns xml for the phone with all SMS messages
- redirects to `/messages/{entry}` when selecting an entry
- calls RC: GET "/rest../~/message-store"

`/messages/{entry}`
- returns xml for a specific SMS message
- calls RC: GET "/rest../~/message-store/{entry}"

`/send-message-prompt`
- returns xml InputScreen
- collects `to` number and `text` query parameters
- redirects to `/send-message`

`/send-message-directory`
- collects `to` number parameter
- provides selection from RC's directory
- redirects to `/send-message-prompt`

`/send-message`
- sends the SMS based on the provided `to` and `text`
- calls RC: POST "/rest../~/sms"

`/direct-messages`
- lists direct chats
- redirects to `/chat/{entry}`
- calls RC: GET "/rest../glip/chats?type=Direct&type=Personal&type=Group"

`/team-messages`
- lists team chats
- redirects to `/chat/{entry}`
- calls RC: GET "/rest../glip/chats?type=Team&type=Everyone"

`/chat/{entry}`
- lists chat messages and a prompt for sending a message
- redirects to `/chat/{entry}/send-prompt`
- calls RC: GET "/rest../glip/chats/{entry}/posts"

`/chat/{entry}/send-prompt`
- collects `text` query parameter
- redirects to `/chat/{entry}/send`

`/chat/{entry}/send`
- calls RC: POST "/rest../glip/chats/posts"

`/directory`
- redirects to `/directory/{entry}`
- calls RC: "/rest../directory/entries"

`/directory/{entry}`
- lists a user corresponding numbers
- calls RC: "/rest../directory/entries/{entry}"

`/call-log`
- calls RC: "/rest../~/call-log"

`/clear-notification`
- clear the notification for a user

### Structure

`authTokens` variable
- keeping each logged-in-user's tokens from the RC SDK

`syncToken` variable
- for notifications
- keeps the token provided by the last call to message-sync to RC

`login` function
- logs a user into RingCentral using the credentials provided by the OMM
- gets the first sync token for the user
- calls RC: GET "/rest../~/message-sync?syncType=FSync"

`sync` and `syncInterval` functions
- uses the user's sync token to call RC
- if RC returns at least one SMS, it adds a notification for the user
  - the notification gets removed by calling `/messages` or `/clear-notification`
- calls RC: GET "/rest../~/message-sync?syncType=ISync"

