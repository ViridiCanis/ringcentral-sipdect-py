#!/usr/bin/env python
import ringcentral
from ringcentral import SDK
import os, sys, logging
from dotenv import load_dotenv
import json
import asyncio
from xml.etree import ElementTree as ET
from fastapi import FastAPI, HTTPException, Response

from xml_interface import *
from axi_client import *

logger = logging.getLogger('index')
logger.setLevel(logging.DEBUG)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.INFO)
formatter = logging.Formatter("[%(asctime)s - %(name)s] %(levelname)s - %(message)s")
handler.setFormatter(formatter)

logger.addHandler(handler)

load_dotenv()

app = FastAPI()

# following comments are to prevent the linter from complaining
# the variables are set during startup before anything can use them
axi: AXIClient = None # type: ignore
platform: ringcentral.sdk.Platform = None # type: ignore

@app.on_event("startup")
async def startup():
    rcsdk = SDK( os.environ.get('RC_CLIENT_ID'),
             os.environ.get('RC_CLIENT_SECRET'),
             os.environ.get('RC_SERVER_URL') )

    global platform
    platform = rcsdk.platform()


    global axi
    # following comment is to prevent the linter from complaining about async __init__ which it can't handle
    axi = await AXIClient(os.environ.get("OMM_URL"), os.environ.get("OMM_PORT")) # type: ignore

    await axi.authenticate(os.environ.get("OMM_USER"), os.environ.get("OMM_PW"))

    asyncio.get_running_loop().create_task(sync_interval())

# stores the tokens for authentication for all currently logged in users
authTokens = {}
# ^ but for synchronisation
syncTokens = {}
# save the id of each user on login to avoid needing to call get it repeatedly
usernum_id_map = {}
has_notification = []

@app.get("/user/user_common.cfg")
async def user_common():
    # use example user_common.cfg
    with open("user_common.cfg") as f:
        return Response(content=f.read())

@app.get("/user/user.cfg")
async def user(user: str, pw: str):
    # logging in with a '+' before the phone number doesn't work
    if user.startswith("+"):
        # unprocessable entity, same as what FastAPI returns
        raise HTTPException(status_code=422)

    platform.login(username=user, password=pw)

    resp = platform.get("/restapi/v1.0/account/~/extension/~/device")
    json_resp = json.loads(resp.response().content)

    # find a BYOD for this account
    device = None
    for record in json_resp["records"]:
        if record["type"] == "OtherPhone":
            device = record
            break

    if device == None:
        logger.error("no OtherPhone found")
        raise HTTPException(status_code=500)

    resp = platform.get("/restapi/v1.0/account/~/extension/~")
    extension = json.loads(resp.response().content)

    resp = platform.get(f'/restapi/v1.0/account/~/device/{device["id"]}/sip-info')
    sip_info = json.loads(resp.response().content)
    
    username = extension["name"]
    sip_username = sip_info["userName"]
    auth_id = sip_info["authorizationId"]
    sip_pw = sip_info["password"]

    response = (f"UD_Numner={sip_username}\n"
                f"UD_Pin=1111\n" # TODO: pin is constant
                f"UD_Name={username}\n"
                f"UD_SipAccount={auth_id}\n"
                f"UD_SipPassword={sip_pw}\n"
                f"UD_ServiceAuthName={user}\n"
                f"UD_ServiceAuthPasswd={pw}\n")

    return Response(content=response)

@app.get("/logout")
async def logout(usernum: str):
    logger.info("GET logout")
    token = authTokens.pop(usernum)

    if token != None:
        platform.auth().set_data(token)
        platform.logout()

    return Response()

@app.get("/messages")
async def get_messages(usernum: str):
    # TODO: add paging, similar endpoints have it, this one doesn't

    logger.info("GET messages")
    token = authTokens.get(usernum)

    try:
        has_notification.remove(usernum)
        await clear_notification(usernum)
    except ValueError:
        # user doesn't have a notification
        pass

    if token == None:
        token = await login(usernum)

    platform.auth().set_data(token)
    if platform.logged_in():
        resp = platform.get("/restapi/v1.0/account/~/extension/~/message-store",
            {"messageType": "SMS", "dateFrom": "2000-01-01"}) # date chosen to get all messages
        json_resp = json.loads(resp.response().content)
        xml = get_messages_xml(json_resp, usernum, os.environ.get("BASE_URL"))
        return Response(content=xml, media_type="application/xml")

    logger.error("not logged in")
    raise HTTPException(status_code=500)

@app.get("/messages/{entry}")
async def get_message_by_id(usernum: str, entry: str):
    logger.info("GET messages/{entry}")
    token = authTokens.get(usernum)

    try:
        has_notification.remove(usernum)
        await clear_notification(usernum)
    except ValueError:
        # user doesn't have a notification
        pass

    if token == None:
        token = await login(usernum)

    platform.auth().set_data(token)
    if platform.logged_in():
        resp = platform.get(f"/restapi/v1.0/account/~/extension/~/message-store/{entry}")

        json_resp = json.loads(resp.response().content)
        xml = get_message_by_id_xml(json_resp, usernum, os.environ.get("BASE_URL"))
        return Response(content=xml, media_type="application/xml")

    logger.error("not logged in")
    raise HTTPException(status_code=500)

@app.get("/send-message-prompt")
async def send_message_prompt(usernum: str, to: str | None = None):
    logger.info("GET send-message-prompt")
    token = authTokens.get(usernum)

    if token == None:
        token = await login(usernum)

    xml = send_message_prompt_xml(usernum, to, os.environ.get("BASE_URL"))
    return Response(content=xml, media_type="application/xml")

# send a message using the directory to select the recipient
@app.get("/send-message-directory")
async def send_message_directory(usernum: str, page: int | None = None):
    logger.info("GET send-message-directory")
    token = authTokens.get(usernum)

    if page == None:
        page = 1

    if token == None:
        token = await login(usernum)

    platform.auth().set_data(token)
    if platform.logged_in():
        resp = platform.get(f"/restapi/v1.0/account/~/directory/entries/?perPage=7&page={page}")

        json_resp = json.loads(resp.response().content)
        xml = send_directory_xml(json_resp, usernum, os.environ.get("BASE_URL"))
        return Response(content=xml, media_type="application/xml")

    logger.error("not logged in")
    raise HTTPException(status_code=500)

@app.get("/send-message-directory/{entry}")
async def send_message_directory_entry(usernum: str, entry: str):
    logger.info("GET send-message-directory/{entry}")
    token = authTokens.get(usernum)

    if token == None:
        token = await login(usernum)

    platform.auth().set_data(token)
    if platform.logged_in():
        resp = platform.get(f"/restapi/v1.0/account/~/directory/entries/{entry}")

        json_resp = json.loads(resp.response().content)
        xml = send_directory_entry_xml(json_resp, usernum, os.environ.get("BASE_URL"))
        return Response(content=xml, media_type="application/xml")

    logger.error("not logged in")
    raise HTTPException(status_code=500)

@app.get("/send-message")
async def send_message(usernum: str, to: str, text: str):
    logger.info("GET send-message")
    token = authTokens.get(usernum)

    if token == None:
        token = await login(usernum)

    platform.auth().set_data(token)
    if platform.logged_in():
        _from = platform.get("/restapi/v1.0/account/~/extension/~/phone-number").response()
        _from = json.loads(_from.content)["records"][0]["phoneNumber"]

        platform.post("/restapi/v1.0/account/~/extension/~/sms",
        {
          "from": { "phoneNumber": _from },
          "to": [{ "phoneNumber": to }],
          "text": text,
        })

        xml = message_sent_xml()
        return Response(content=xml, media_type="application/xml")

    logger.error("not logged in")
    raise HTTPException(status_code=500)

# TODO: batch requests using a list of ids
def get_account(_id):
    resp = platform.get(f"/restapi/v1.0/glip/persons/{_id}")
    resp = resp.response().content
    return json.loads(resp)

@app.get("/direct-messages")
async def direct_messages(usernum: str, pageToken: str | None = None):
    logger.info("GET direct-messages")
    token = authTokens.get(usernum)

    if token == None:
        token = await login(usernum)

    platform.auth().set_data(token)
    if platform.logged_in():
        endpoint = "/restapi/v1.0/glip/chats?recordCount=7&type=Direct&type=Personal&type=Group"

        if pageToken != None:
            endpoint += f"&pageToken={pageToken}"

        resp = platform.get(endpoint)
        json_resp = json.loads(resp.response().content)

        data = []

        for record in json_resp["records"]:
            text = ""

            _type = record["type"]
            if _type == "Direct":
                text = "Direct to "

                for member in record["members"]:
                    _id = member.get("id")
                    if _id != None and _id != usernum_id_map[usernum]:
                        acc = get_account(_id)
                        text += acc["firstName"] + " " + acc["lastName"]
                        
            elif _type == "Group":
                users = []
                for member in record["members"]:
                    _id = member.get("id")
                    if _id != None and _id != usernum_id_map[usernum]:
                        acc = get_account(_id)
                        users.append(acc["firstName"] + " " + acc["lastName"])

                text = f"Group: {', '.join(users)}"
            else:
                text = _type

            data.append({
                "id": record["id"],
                "text": text,
            })

        nav = json_resp.get("navigation")
        pageToken = None
        if nav != None:
            pageToken = nav.get("nextPageToken")

        xml = direct_messages_xml(data, pageToken, usernum, os.environ.get("BASE_URL"))

        return Response(content=xml, media_type="application/xml")

    logger.error("not logged in")
    raise HTTPException(status_code=500)

@app.get("/team-messages")
async def team_messages(usernum: str, pageToken: str | None = None):
    logger.info("GET team-messages")
    token = authTokens.get(usernum)

    if token == None:
        token = await login(usernum)

    platform.auth().set_data(token)
    if platform.logged_in():
        endpoint = "/restapi/v1.0/glip/chats?recordCount=7&type=Team&type=Everyone"

        if pageToken != None:
            endpoint += f"&pageToken={pageToken}"

        resp = platform.get(endpoint)
        json_resp = json.loads(resp.response().content)

        data = []

        for record in json_resp["records"]:
            data.append({
                "id": record["id"],
                "text": record["name"],
            })

        nav = json_resp.get("navigation")
        pageToken = None
        if nav != None:
            pageToken = nav.get("nextPageToken")

        xml = team_messages_xml(data, pageToken, usernum, os.environ.get("BASE_URL"))

        return Response(content=xml, media_type="application/xml")

    logger.error("not logged in")
    raise HTTPException(status_code=500)

@app.get("/chat/{entry}")
async def chat(usernum: str, entry: str, pageToken: str | None = None):
    logger.info("GET chat/{entry}")
    token = authTokens.get(usernum)

    if token == None:
        token = await login(usernum)

    platform.auth().set_data(token)
    if platform.logged_in():
        endpoint = f"/restapi/v1.0/glip/chats/{entry}/posts"
    
        if pageToken != None:
            endpoint += f"?pageToken={pageToken}"

        resp = platform.get(endpoint)
        json_resp = json.loads(resp.response().content)
        pageToken = json_resp["navigation"].get("nextPageToken")

        data = []
        for record in json_resp["records"]:
            if record["type"] == "TextMessage":
                acc = get_account(record["creatorId"])

                data.append({
                    "from": acc["firstName"] + " " + acc["lastName"],
                    "text": record["text"]
                })

        xml = chat_xml(data, entry, pageToken, usernum, os.environ.get("BASE_URL"))

        return Response(content=xml, media_type="application/xml")

    logger.error("not logged in")
    raise HTTPException(status_code=500)

@app.get("/chat/{entry}/send-prompt")
async def chat_send_prompt(usernum: str, entry: str):
    logger.info("GET chat/{entry}")
    token = authTokens.get(usernum)

    if token == None:
        token = await login(usernum)

    xml = chat_send_prompt_xml(entry, usernum, os.environ.get("BASE_URL"))

    return Response(content=xml, media_type="application/xml")

@app.get("/chat/{entry}/send")
async def chat_send(usernum: str, entry: str, text: str):
    logger.info("GET chat/{entry}")
    token = authTokens.get(usernum)

    if token == None:
        token = await login(usernum)

    platform.auth().set_data(token)
    if platform.logged_in():
        platform.post(f"/restapi/v1.0/glip/chats/{entry}/posts", {
                'text': text
            })

        xml = message_sent_xml()
        return Response(content=xml, media_type="application/xml")

    logger.error("not logged in")
    raise HTTPException(status_code=500)

@app.get("/directory")
async def directory(usernum: str, page: int | None = None):
    logger.info("GET directory")
    token = authTokens.get(usernum)

    if page == None:
        page = 1

    if token == None:
        token = await login(usernum)

    platform.auth().set_data(token)
    if platform.logged_in():
        resp = platform.get(f"/restapi/v1.0/account/~/directory/entries/?perPage=7&page={page}")

        json_resp = json.loads(resp.response().content)
        xml = directory_xml(json_resp, usernum, os.environ.get("BASE_URL"))
        return Response(content=xml, media_type="application/xml")

    logger.error("not logged in")
    raise HTTPException(status_code=500)

@app.get("/directory/{entry}")
async def directory_entry(usernum: str, entry: str):
    logger.info("GET directory/{entry}")
    token = authTokens.get(usernum)

    if token == None:
        token = await login(usernum)

    platform.auth().set_data(token)
    if platform.logged_in():
        resp = platform.get(f"/restapi/v1.0/account/~/directory/entries/{entry}")

        json_resp = json.loads(resp.response().content)
        xml = directory_entry_xml(json_resp)
        return Response(content=xml, media_type="application/xml")

    logger.error("not logged in")
    raise HTTPException(status_code=500)

@app.get("/call-log")
async def call_log(usernum: str, page: int | None = None):
    logger.info("GET call-log")
    token = authTokens.get(usernum)

    if page == None:
        page = 1

    if token == None:
        token = await login(usernum)

    platform.auth().set_data(token)
    if platform.logged_in():
        resp = platform.get(f"/restapi/v1.0/account/~/extension/~/call-log?perPage=7&page={page}")

        json_resp = json.loads(resp.response().content)
        xml = call_log_xml(json_resp, usernum, os.environ.get("BASE_URL"))
        return Response(content=xml, media_type="application/xml")

    logger.error("not logged in")
    raise HTTPException(status_code=500)

async def clear_notification(usernum: str):
    try:
        has_notification.remove(usernum)
    except ValueError:
        # user doesn't have a notification
        pass

    inner = notification_xml("")
    req = ET.fromstring(f"""<PushXML num="{usernum}" />""")
    req.attrib["xmlDoc"] = inner
    if axi != None:
        await axi.request(req)
    else:
        raise Exception("AXI client not initialized")

@app.get("/clear-notification")
async def clear_notification_endpoint(usernum: str):
    await clear_notification(usernum)
    return Response(content=notification_xml(""), media_type="application/xml")

async def login(usernum: str):
    logger.info("logging in")

    req = ET.fromstring(f'<GetPPUserByNumber num="{usernum}" />')
    if axi != None:
        res = await axi.request(req)
        user = res.find("./user")
        if user != None:
            username = user.attrib["serviceAuthName"]
            password = user.attrib["serviceAuthPassword"]

            platform.login(username=username, password=password)
            tokens = platform.auth().data()
            authTokens[usernum] = tokens
        else:
            logger.error("couldn't find user")
            raise HTTPException(status_code=500)
    else:
        raise HTTPException(status_code=500)

    resp = platform.get("/restapi/v1.0/account/~/extension/~/message-sync", {
        "syncType": "FSync",
        "messageType": "SMS"
    })
    json_resp = json.loads(resp.response().content)
    syncTokens[usernum] = json_resp["syncInfo"]["syncToken"]

    resp = platform.get("/restapi/v1.0/account/~")
    json_resp = json.loads(resp.response().content)
    usernum_id_map[usernum] = str(json_resp["id"])

    return tokens

async def sync():
    for usernum, token in authTokens.items():
        platform.auth().set_data(token)
        if platform.logged_in():
            resp = platform.get("/restapi/v1.0/account/~/extension/~/message-sync", {
                        "syncType": "ISync",
                        "syncToken": syncTokens[usernum]
                    }
            )

            json_resp = json.loads(resp.response().content)
            syncTokens[usernum] = json_resp["syncInfo"]["syncToken"]

            if len(json_resp["records"]):
                try:
                    has_notification.index(usernum)
                    # user already has a notification if a exception hasn't been thrown
                except ValueError:
                    inner = notification_xml("new message")
                    logger.debug(f"""<PushXML num="{usernum}" />""")
                    req = ET.fromstring(f"""<PushXML num="{usernum}" />""")
                    req.attrib["xmlDoc"] = inner
                    if axi != None:
                        await axi.request(req)
                    else:
                        raise HTTPException(status_code=500)

                    has_notification.append(usernum)

async def sync_interval():
    while True:
        await asyncio.sleep(60)
        logger.info("starting sync")
        await sync()

@app.on_event("shutdown")
def shutdown():
    logger.info("Quitting")

    if axi != None:
        axi.shutdown()

