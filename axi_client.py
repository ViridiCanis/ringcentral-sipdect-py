import asyncio
import logging
from xml.etree import ElementTree as ET
from aobject import aobject

class AXIClient(aobject):
    async def __init__(self, omm_ip, omm_port):
        self.reader, self.writer = await asyncio.open_connection(omm_ip, omm_port)

        self.callbacks = {}
        self._seq = 0
        self.chunk = ""
        self.public_key = None
        self.read_loop = asyncio.create_task(self.loop())
        self.keep_alive_loop = None

    def shutdown(self):
        self.read_loop.cancel()
        if self.keep_alive_loop != None:
            self.keep_alive_loop.cancel()

    async def loop(self):
        while True:
            try:
                msg = await self.reader.read(1024)
                self.chunk += msg.decode()

                # if the message is not complete
                if not self.chunk.endswith("\0"):
                    continue

                xml = ET.fromstring(self.chunk[:-1])
                self.chunk = ""
                seq = xml.attrib["seq"]

                self.callbacks[seq](xml)
            except Exception as e:
                logging.error(f"axi read loop")
                logging.error(e)

    # does a ping every few minutes to keep the connection alive
    async def keep_alive(self):
        while True:
            await asyncio.sleep(4.8 * 60)
            req = '<Ping />'
            await self.request(ET.fromstring(req))

    # helper method for the sequence number
    def seq(self):
        self._seq += 1
        return self._seq

    # helper method
    def callback(self, fut: asyncio.Future, res: ET.Element):
        fut.set_result(res)

    # internal method for making a request with a specific seq
    def _request(self, seq, req):
        fut = asyncio.Future()
        self.callbacks[str(seq)] = lambda res: self.callback(fut, res)

        self.writer.write(req + b'\0')
        return fut

    async def authenticate(self, username, password):
        seq = self.seq()
        resp: ET.Element = await self._request(seq, bytes(f"""<Open username="{username}" password="{password}" seq="{seq}" />""", "utf-8"))

        self.public_key = resp.find("./publicKey")

        self.keep_alive_loop = asyncio.create_task(self.keep_alive())

    async def request(self, request: ET.Element) -> ET.Element:
        seq = self.seq()

        request.attrib["seq"] = str(seq)
        return await self._request(seq, ET.tostring(request))
