from urllib.parse import quote_plus

def error_login_xml():
    return """<?xml version="1.0" encoding="UTF-8"?>
    <AastraIPPhoneTextScreen>
      <Title>RC SMS</Title>
      <Text>failed logging in</Text>
    </AastraIPPhoneTextScreen>
    """

def phoneNumber_or_name(json: dict):
    if json.get("name"):
        return json["name"]
    else:
        return " " + json["phoneNumber"]

def login_xml(base_url, usernum: str | None, loginname: str | None):
    t = None
    if loginname == None:
        t = "number"
    else:
        t = "string"

    xml = f"""<?xml version="1.0" encoding="UTF-8"?>
    <AastraIPPhoneInputScreen type="{t}">"""
    xml += "<Title>RC login</Title>"

    if loginname == None:
        xml += f"""<URL>{base_url}/login?usernum={usernum}</URL>
        <Prompt>Email or Phone Number</Prompt>
        <Parameter>loginname</Parameter>"""
    else:
        xml += f"""<URL>{base_url}/login?usernum={usernum}&loginname={loginname}</URL>
        <Prompt>Password</Prompt>
        <Parameter>password</Parameter>"""

    return xml + "</AastraIPPhoneInputScreen>"

def get_messages_xml(json: dict, usernum, base_url):
    xml = """<?xml version="1.0" encoding="UTF-8"?>
  <AastraIPPhoneTextMenu defaultIndex="1">
    <Title>RC SMS</Title>
    """

    for message in json["records"]:
        xml += f"""<MenuItem>
            <URI>{base_url}/messages/{message["id"]}?usernum={usernum}</URI>
            <Prompt>{phoneNumber_or_name(message["from"])}</Prompt>
        </MenuItem>
        """

    xml += "</AastraIPPhoneTextMenu>"
    return xml

def get_message_by_id_xml(json: dict, usernum, base_url):
      return f"""<?xml version="1.0" encoding="UTF-8"?>
        <AastraIPPhoneTextScreen>
        <Title>{phoneNumber_or_name(json["from"])}</Title>
        <Text>{json["subject"]}</Text>
        <SoftKey index="1">
            <Label>Delete</Label>
            <URI>{base_url}/get-messages?usernum={usernum}</URI>
        </SoftKey>
        </AastraIPPhoneTextScreen>"""

def send_message_prompt_xml(usernum, to: str | None, base_url):
    xml = f"""<?xml version="1.0" encoding="UTF-8"?>
    <AastraIPPhoneInputScreen type=\""""

    if to != None:
        xml += "string"
    else:
        xml += "number"

    xml += "\"><Title>RC SMS</Title>"

    if to == None:
        xml += f"""<URL>{base_url}/send-message-prompt?usernum={usernum}</URL>
        <Prompt>Receiving Number</Prompt>
        <Parameter>to</Parameter>"""
    else:
        xml += f"""<URL>{base_url}/send-message?usernum={usernum}&to={to}</URL>
        <Prompt>Text</Prompt>
        <Parameter>text</Parameter>"""

    return xml + "</AastraIPPhoneInputScreen>"

def message_sent_xml():
    return """<?xml version="1.0" encoding="UTF-8"?>
  <AastraIPPhoneTextScreen>
    <Title>RC SMS</Title>
    <Text>message sent</Text>
  </AastraIPPhoneTextScreen>"""

def directory_xml(json: dict, usernum, base_url):
    xml = """<?xml version="1.0" encoding="UTF-8"?>
<AastraIPPhoneTextMenu defaultIndex="1" destroyOnExit="yes" Timeout="50">
  <Title wrap="no"> Directory</Title>"""

    for record in json["records"]:
        xml += """<MenuItem base="">"""
        if record.get("firstName") != None:
            xml += f"""<Prompt>{record["firstName"]} {record["lastName"]}</Prompt>"""
        else:
            xml += f"""<Prompt>{record["name"]}</Prompt>"""

        xml += f"""<URI>{base_url}/directory/{record["id"]}?usernum={usernum}</URI>"""

        xml += """<Selection>info</Selection>
        </MenuItem>"""

    if (int(json["paging"]["page"]) < int(json["paging"]["totalPages"])):
        xml += f"""<MenuItem>
            <Prompt>Next Page</Prompt>
            <URI>{base_url}/directory?usernum={usernum}&page={int(json["paging"]["page"]) + 1}</URI>
        </MenuItem>"""

    return xml + "</AastraIPPhoneTextMenu>"

def directory_entry_xml(json):
    xml = """<?xml version="1.0" encoding="UTF-8"?>
        <AastraIPPhoneTextMenu defaultIndex="1" destroyOnExit="yes" Timeout="50">
        <Title wrap="no">"""

    if json.get("firstName") != None:
        xml += f"""{json["firstName"]} {json["lastName"]}"""
    else:
        xml += json["name"]

    xml += "</Title>"

    if json.get("phoneNumbers") != None:
        for phone in json["phoneNumbers"]:
            xml += """<MenuItem base="">
            <Prompt>"""

            if phone.get("label") != None:
                xml += phone["label"]
            else:
                xml += phone["type"]

            xml += f"""{phone["formattedPhoneNumber"]}"""

            xml += f"""</Prompt>
            <Selection>info</Selection>
            <Dial>{phone["phoneNumber"]}</Dial>
            </MenuItem>"""

    return xml + "</AastraIPPhoneTextMenu>"

def send_directory_xml(json, usernum, base_url):
    xml = """<?xml version="1.0" encoding="UTF-8"?>
<AastraIPPhoneTextMenu defaultIndex="1" destroyOnExit="yes" Timeout="50">
  <Title wrap="no"> Directory</Title>"""

    for record in json["records"]:
        xml += """<MenuItem base="">"""
        if record.get("firstName") != None:
            xml += f"""<Prompt>{record["firstName"]} {record["lastName"]}</Prompt>"""
        else:
            xml += f"""<Prompt>{record["name"]}</Prompt>"""

        xml += f"""<URI>{base_url}/send-message-directory/{record["id"]}?usernum={usernum}</URI>"""

        xml += """<Selection>info</Selection>
        </MenuItem>"""

    if (int(json["paging"]["page"]) < int(json["paging"]["totalPages"])):
        xml += f"""<MenuItem>
            <Prompt>Next Page</Prompt>
            <URI>{base_url}/send-message-directory?usernum={usernum}&page={int(json["paging"]["page"]) + 1}</URI>
        </MenuItem>"""

    return xml + "</AastraIPPhoneTextMenu>"

def send_directory_entry_xml(json, usernum, base_url):
    xml = """<?xml version="1.0" encoding="UTF-8"?>
        <AastraIPPhoneTextMenu defaultIndex="1" destroyOnExit="yes" Timeout="50">
        <Title wrap="no">"""

    if json.get("firstName") != None:
        xml += f"""{json["firstName"]} {json["lastName"]}"""
    else:
        xml += json["name"]

    xml += "</Title>"

    if json.get("phoneNumbers") != None:
        for phone in json["phoneNumbers"]:
            xml += """<MenuItem base="">
            <Prompt>"""

            if phone.get("label") != None:
                xml += phone["label"]
            else:
                xml += phone["type"]

            xml += f"""{phone["formattedPhoneNumber"]}"""

            xml += f"""</Prompt>
            <URI>{base_url}/send-message-prompt?usernum={usernum}&to={quote_plus(phone["phoneNumber"])}</URI>
            </MenuItem>"""

    return xml + "</AastraIPPhoneTextMenu>"

def call_log_xml(json, usernum, base_url):
    xml = """<?xml version="1.0" encoding="UTF-8"?>
    <AastraIPPhoneTextMenu defaultIndex="1" destroyOnExit="yes" Timeout="50">
    <Title wrap="no">Call Log</Title>"""

    for record in json["records"]:
        xml += """<MenuItem base="">
        <Prompt>"""

        if record["direction"] == "Inbound":
            xml += f"""i {phoneNumber_or_name(record["from"])}"""
        else:
            xml += f"""o {phoneNumber_or_name(record["to"])}"""

        xml += "</Prompt>"

        xml += "<Dial>"
        if record["direction"] == "Inbound":
            xml += record["from"]["phoneNumber"];
        else:
            xml += record["to"]["phoneNumber"];

        xml += "</Dial>"

        xml += """<Selection>info</Selection>
        </MenuItem>"""

    if int(json["paging"]["page"]) < int(json["paging"]["totalPages"] if json["paging"].get("totalPages") != None else 1):
        xml += f"""<MenuItem>
        <Prompt>Next Page</Prompt>
        <URI>{base_url}/call-log?usernum={usernum}&page={int(json["paging"]["page"]) + 1}</URI>
        </MenuItem>"""

    return xml + "</AastraIPPhoneTextMenu>"

def notification_xml(value):
    return f"""<?xml encoding="utf-8" version="1" ?><AastraIPPhoneConfiguration><ConfigurationItem><Parameter>Set idle line</Parameter><Value>{value}</Value></ConfigurationItem></AastraIPPhoneConfiguration>"""

def direct_messages_xml(data, pageToken, usernum, base_url):
    xml = """<?xml version="1.0" encoding="UTF-8"?>
    <AastraIPPhoneTextMenu defaultIndex="1" destroyOnExit="yes" Timeout="50">
    <Title wrap="no">Direct Messages</Title>"""

    for record in data:
        xml += f"""<MenuItem base="">
        <Prompt>{record["text"]}</Prompt>
        <URI>{base_url}/chat/{record["id"]}?usernum={usernum}</URI>
        </MenuItem>"""

    if pageToken != None:
        xml += f"""<MenuItem base="">
        <Prompt>Next Page</Prompt>
        <URI>{base_url}/direct-messages?pageToken={pageToken}&usernum={usernum}</URI>
        </MenuItem>
        """

    return xml + "</AastraIPPhoneTextMenu>"

def team_messages_xml(data, pageToken, usernum, base_url):
    xml = """<?xml version="1.0" encoding="UTF-8"?>
    <AastraIPPhoneTextMenu defaultIndex="1" destroyOnExit="yes" Timeout="50">
    <Title wrap="no">Team Messages</Title>"""

    for record in data:
        xml += f"""<MenuItem base="">
        <Prompt>{record["text"]}</Prompt>
        <URI>{base_url}/chat/{record["id"]}?usernum={usernum}</URI>
        </MenuItem>"""

    if pageToken != None:
        xml += f"""<MenuItem base="">
        <Prompt>Next Page</Prompt>
        <URI>{base_url}/team-messages?pageToken={pageToken}&usernum={usernum}</URI>
        </MenuItem>
        """

    return xml + "</AastraIPPhoneTextMenu>"

def chat_xml(data, chat_id, pageToken, usernum, base_url):
    xml = f"""<?xml version="1.0" encoding="UTF-8"?>
  <AastraIPPhoneTextMenu defaultIndex="1">
    <Title>RC Chat</Title>

    <MenuItem base="">
        <Prompt>Send message</Prompt>
        <URI>{base_url}/chat/{chat_id}/send-prompt?usernum={usernum}</URI>
    </MenuItem>
    """

    for message in data:
        xml += f"""<MenuItem>
            <Prompt>{message["from"]}: {message["text"]}</Prompt>
        </MenuItem>
        """

    if pageToken != None:
        xml += f"""<MenuItem>
            <Prompt>Next Page</Prompt>
            <URI>{base_url}/chat/{chat_id}?usernum={usernum}&pageToken={pageToken}</URI>
            </MenuItem>"""

    xml += "</AastraIPPhoneTextMenu>"
    return xml

def chat_send_prompt_xml(entry, usernum, base_url):
    return f"""<?xml version="1.0" encoding="UTF-8"?>
    <AastraIPPhoneInputScreen type="string">
    <Title>RC SMS</Title>

    <URL>{base_url}/chat/{entry}/send?usernum={usernum}</URL>
    <Prompt>Text</Prompt>
    <Parameter>text</Parameter>

    </AastraIPPhoneInputScreen>
    """
