# Ringcentral SIP-DECT Integration in Python

## Setup

### Ringcentral App

Go to your Ringcentral Developer Account and register create an App with the
following settings:

- App Type: REST Api App
- Auth:
  - Password-based auth flow
  - Issue refresh token: yes
- Security -> App Permissions: Read Accounts, Read Messages, SMS, Read Call Log, Team Messaging

then go to the Dashboard of the App and copy the Client ID and the Client Secret
into the file `.env` to the corresponding variables `RC_CLIENT_ID` and `RC_CLIENT_SECRET`

#### Users

Each user should have a Existing Phone/BYOD

with TLS 1.2

| Mitel SIP-DECT System  |  Ring Central SIP Settings      |
| SIP Settings           |  (Setup & Provisioning – Manual |
|                        |  Provisioning)                  |
|------------------------|---------------------------------|
| Proxy Server           |  SIP Domain w/o port            |
| Proxy Port             |  SIP Domain port                |
| Registrar Server       |  SIP Domain w/o port            |
| Registrar Port         |  SIP Domain port                |
| Outbound proxy server  |  Outbound Proxy                 |
| Outbound proxy port    |  Outbound Proxy Port            |

### OMM Configuration

Provisioning:
Validate certificates: off

SIP -> Basic Settings:
Transport Protocol: Persistent TLS
Globally routable user agent URL: off

SIP -> Security:
Send SIPS over TLS active: off
Validate certificates: off
Advanced Settings -> Security:
Security Level & Cipher suites ...: Legacy

Sites:
Wideband Audio G.722: on
SRTP: Only

### OMM

in `.env` set:

if running the server on the OMM:
- have [ip of your pc] be the OMM's ip address
- have [ip of the omm] be `localhost`

enable the unencrypted tcp port on the OMM
in ommconsole:
`cnf sys uap on`

```
BASE_URL  = "http://[ip of your pc]:5000";
OMM_URL   = "[ip of the omm]";
OMM_USER  = "[omm user]";
OMM_PW    = "[omm password]";
```

make sure the firewall of your pc allows TCP on port 5000

for all the following settings set the port to 5000

add to XML Applications your pc as the server with protocol set to HTTP
and set the path respectively to:
- for logging out:        `logout?usernum={number}`

for messaging based on SMS:
- for reading messages:   `messages?usernum={number}`
- for sending a message:  `send-message-prompt?usernum={number}`

for messaging based on RC's messaging:
- for direct messages: `direct-messages?usernum={number}`
- for team messages: `team-messages?usernum={number}`

for directory:
- make an entry with type XML and Path `directory?usernum={number}`
- send a message based on the directory, with the same settings as send-message-prompt `send-message-directory?usernum={number}`

for call logs:
- change Caller List and Redial List in XML Applications to the Path `call-log?usernum={number}`

### User Login

in Provision, set User Data Import to
configure specific source: yes
protocol: HTTP
port: 5000
url: [ip of the pc]
path: user

in Provision -> Event Trigger:
set User Data to O and trigger it once (e.g. manual trigger)

when logging in using the phone number, do it without a starting `+`

the password needs to be something which can be typed using the phone
and it can't contain characters with special meaning within an URL (e.g. + / #)

### Running the server on the OMM

#### Python

move cross-compiled python directory to the OMM at `/var/opt/python`

after every reboot:
`ln -s /var/opt/python/bin/python3 /bin/`

#### Dependencies

move `libs.tar.gz` to the OMM
do `tar xf lib.tar.gz -C /var/opt/python`

move `uvicorn` to the OMM in `/var/opt`

after every reboot:
`ln -s /var/opt/uvicorn /bin/`

#### Program

move the project's files to the OMM
for example to `/tmp/project`
(.env, all .py files, and the user\_common.cfg)

#### Running

from the directory where the python scripts (e.g. `/tmp/project`) are in:
`env PYTHONPATH=/var/opt/python/lib64/python3.10/site-packages uvicorn index:app --port 5000 --host 0.0.0.0`

## Usage

start virtual environment using pipenv:

```
pipenv install
pipenv shell
```

run using
`uvicorn index:app --reload --port 5000 --host 0.0.0.0`

