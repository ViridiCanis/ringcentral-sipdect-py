set windows-powershell

default:
    just --list

install-deps:
    checkexec Pipfile.lock Pipfile -- pipenv install

pipenv: install-deps
    pipenv shell

watch:
    uvicorn index:app --reload --port 5000 --host 0.0.0.0

run:
    unicorn index:app --port 5000 --host 0.0.0.0
